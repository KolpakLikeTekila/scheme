package sample.Elements;

import sample.DataCharacters.Data;

/**
 * Created by Колпаков Антон on 24.06.2016.
 */
public class EMF extends Data{
    public double E;
    public double R;
    public double timeConnect;
    public double timeWork;

    public EMF(double _E, double _R, double tCon, double tWork, String _name){
        E=_E;
        R=_R;
        timeConnect = tCon;
        timeWork = tWork;
        NameElement=_name;
    }

    public double getE(){
        return  E;
    }

    public void setE(double _E){
        E=_E;
    }

    public double getR(){
        return  R;
    }

    public void setR(double _R){
        R=_R;
    }

    public double getTimeConnect(){
        return  timeConnect;
    }

    public void setTimeConnect(double _TimeConnect){
        timeConnect=_TimeConnect;
    }

    public double getTimeWork(){
        return  timeWork;
    }

    public void setTimeWork(double _TimeWork){
        timeWork=_TimeWork;
    }

    public double CalcU(double Uin, double I){
        double Uout;
        Uout = Uin + E - I*R;
        return Uout;
    }

}
