package sample.Elements;

import sample.DataCharacters.Data;

/**
 * Created by Колпаков Антон on 24.06.2016.
 */
public class Resistor extends Data {

    public double R;
    public double Uin;
    public double Uout;

    public double getUin(){
        return  Uin;
    }

    public void setUin(double _Uin){
        Uin=_Uin;
    }

    public double getUout(){
        return  Uout;
    }

    public void setUout(double _Uout){
        Uout=_Uout;
    }

    public Resistor( double _R, String _name){
        R=_R;
        Uin=0;
        Uout=0;
        NameElement=_name;
    }

    public double getR(){
        return  R;
    }

    public void setR(double _R){
        R=_R;
    }

    public void calcUout(double I){
        Uout = Uin - I*R;
    }

    public double CalcI(){
        double I;
        I = (Uin-Uout)/R;
        return I;
    }
    public double calcI(double U){
        double I;
        I = U/R;
        return I;
    }
}
