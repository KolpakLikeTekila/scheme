package sample.Elements;

import sample.DataCharacters.PosInGrid;

import java.util.ArrayList;

/**
 * Created by Колпаков Антон on 03.07.2016.
 */
public class Circuit {
    PosInGrid Input;
    PosInGrid Pos;
    ArrayList<PosInGrid> OutPut;;
    ArrayList<ArrayList> OutPuts;;
    final String TYPE_IMG_L= "L";
    final String TYPE_IMG_X= "X";
    final String TYPE_IMG_I= "I";
    final String TYPE_IMG_T= "T";
    final String TYPE_IMG_ELEMENT= "ELEMENT";
    String type;
    double rotate;

    public Circuit(int col, int row, double _rotate, String _type){
        Pos = new PosInGrid(col,row);
        Input = new  PosInGrid(col,row);
        OutPut = new ArrayList<>();
        OutPuts = new ArrayList<>();;
        type=_type;
        rotate = _rotate;
    }

    public  void setInput(int col, int row){
        Input = new  PosInGrid(col,row);
    }

    public String getType(){
        return type;
    }

    public  PosInGrid getInput(){
        return Input;
    }

    public void setRotate(double _rotate){
        rotate+=_rotate;
        if (rotate >=360){
            rotate =0;
        }
        if (rotate<0){
            rotate = 360+rotate;
        }

    }

    public ArrayList getListOutput(){
        return OutPut;
    }

    private void outputPathL(int colV, int rowV){
        PosInGrid Element = null;
        if (colV ==0 && rowV ==0) {
            Element = new PosInGrid(Pos.getPosCol() + 1, Pos.getPosRow());
        }
        else  {
            if (rowV ==0){
                if (Math.abs(rotate/90%2)==0){
                    Element = new PosInGrid(Pos.getPosCol(), colV>0?1 + Pos.getPosRow():Pos.getPosRow()-1);
                }
                else {
                    Element = new PosInGrid(Pos.getPosCol(),  colV>0? Pos.getPosRow()-1:Pos.getPosRow()+1);
                }
            }
            else if (colV == 0) {
                if (Math.abs(rotate/90%2)==0){
                    Element = new PosInGrid(rowV>0? 1+ Pos.getPosCol():Pos.getPosCol()-1, Pos.getPosRow());
                }
                else {
                    Element = new PosInGrid(rowV>0? Pos.getPosCol()-1:Pos.getPosCol()+1,  Pos.getPosRow());
                }
            }
        }
        OutPut.add(Element);
    }
    private void outputPathT(int colV, int rowV) {
        PosInGrid ElementLeft = new PosInGrid(Pos.getPosCol() - 1, Pos.getPosRow());
        PosInGrid ElementRight = new PosInGrid(Pos.getPosCol() + 1, Pos.getPosRow());
        PosInGrid ElementUp = new PosInGrid(Pos.getPosCol(), Pos.getPosRow() - 1);
        PosInGrid ElementDown = new PosInGrid(Pos.getPosCol(), Pos.getPosRow() +1);

        PosInGrid[][][] masOutput = {
                {{}, {ElementLeft, ElementDown}, {ElementLeft, ElementRight}, {ElementDown, ElementRight}},
                {{ElementLeft, ElementDown}, {}, {ElementUp, ElementLeft}, {ElementUp, ElementDown}},
                {{ElementLeft, ElementRight}, {ElementUp, ElementLeft}, {}, {ElementUp, ElementRight}},
                {{ElementDown, ElementRight}, {ElementUp, ElementDown}, {ElementUp, ElementRight}, {}}
        };

        int row = (int) rotate / 90;
        int col = 0;
        if (rowV == 0) {
            if (colV < 0) {
                col = 1;
            } else {
                col = 3;
            }
        } else if (rowV < 0) {
            col = 2;
        }
        for (int i = 0; i < 2; i++) {
            OutPut.add(masOutput[row][col][i]);
        }
     //   OutPuts.add(OutPut);
      //  if (OutPuts.size()!=1){
       //     outputCalc();
      //  }
    }

   /* private void outputCalc(){
        ArrayList<PosInGrid> list1 = OutPuts.get(0);
        ArrayList<PosInGrid> list3 = new ArrayList<>();
        for(int i = 1; i< OutPuts.size();i++)
        {
            ArrayList<PosInGrid> list2 = OutPuts.get(i);
            for(int j=0;j<list1.size();j++)
                for(int k =0; k<list2.size();k++){
                   if(list1.get(j)==list2.get(k)){
                       list3.add(list2.get(k));
                }
            }
            list1 = new ArrayList<>(list3);
        list3.clear();
        }
        OutPut.clear();
        OutPut = new ArrayList<>(list1);
    }**/

    private void outputPathX(int colV, int rowV) {
        if (colV > 0) {
            OutPut.add(new PosInGrid(Pos.getPosCol(), Pos.getPosRow() - 1));
            OutPut.add(new PosInGrid(Pos.getPosCol(), Pos.getPosRow() + 1));
            OutPut.add(new PosInGrid(Pos.getPosCol() + 1, Pos.getPosRow()));
        } else if (colV < 0) {
            OutPut.add(new PosInGrid(Pos.getPosCol(), Pos.getPosRow() - 1));
            OutPut.add(new PosInGrid(Pos.getPosCol() - 1, Pos.getPosRow()));
            OutPut.add(new PosInGrid(Pos.getPosCol(), Pos.getPosRow() + 1));
        } else if (rowV > 0) {
            OutPut.add(new PosInGrid(Pos.getPosCol() - 1, Pos.getPosRow()));
            OutPut.add(new PosInGrid(Pos.getPosCol(), Pos.getPosRow() + 1));
            OutPut.add(new PosInGrid(Pos.getPosCol() + 1, Pos.getPosRow()));
        } else {
            OutPut.add(new PosInGrid(Pos.getPosCol(), Pos.getPosRow() - 1));
            OutPut.add(new PosInGrid(Pos.getPosCol() - 1, Pos.getPosRow()));
            OutPut.add(new PosInGrid(Pos.getPosCol() + 1, Pos.getPosRow()));
        }
    }
    private void outputPathI(int colV, int rowV) {
        PosInGrid Element = null;
        if (colV ==0 && rowV ==0) {
            if (rotate / 90 % 2 == 0) {
                Element = new PosInGrid(Pos.getPosCol(), 1 + Pos.getPosRow());
            } else {
                Element = new PosInGrid(1 + Pos.getPosCol(), Pos.getPosRow());
            }
        }
        else {
            if (colV==0){
                Element = new PosInGrid(Pos.getPosCol(), rowV>0?1 + Pos.getPosRow():Pos.getPosRow()-1);
            }
            else if (rowV==0){
                Element = new PosInGrid(colV>0? 1+ Pos.getPosCol():Pos.getPosCol()-1, Pos.getPosRow());
            }
        }
        OutPut.add(Element);
    }

    public void buildOutput(){
        int colV =Pos.getPosCol() - Input.getPosCol();
        int rowV = Pos.getPosRow()-Input.getPosRow();
        OutPut.clear();
        switch (type) {
            case TYPE_IMG_T:
                outputPathT(colV,rowV);
                break;
            case TYPE_IMG_X:
                outputPathX(colV,rowV);
                break;
            case TYPE_IMG_L:
                outputPathL(colV,rowV);
                break;
            default:
                outputPathI(colV,rowV);
                break;
        }

    }
}
