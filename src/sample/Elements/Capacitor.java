package sample.Elements;

import sample.DataCharacters.Data;

/**
 * Created by Колпаков Антон on 24.06.2016.
 */
public class Capacitor extends Data {
    public double C;
    public double Uin;
    public double Uout;

    public double getUin(){
        return  Uin;
    }

    public void setUin(double _Uin){
        Uin=_Uin;
    }

    public double getUout(){
        return  Uout;
    }

    public void setUout(double _Uout){
        Uout=_Uout;
    }

    public Capacitor( double _C, String _name){
        C=_C;
        Uin=0;
        Uout=0;
        NameElement=_name;
    }

    public double getC(){
        return  C;
    }

    public void setC(double _C){
        C=_C;
    }


    public double disU(double time, double U, double R){
        double u;
        u = U*(Math.exp(-time/(R*C)));
        return u;
    }

    public double accumU(double time, double U, double R)
    {
        double u;
        u = U*(1-Math.exp(-time/(R*C)));
        return u;
    }

    public double disI(double time, double U, double R){
        double i;
        i = disU(time, U, R)/R;
        return i;
    }

    public double accumI(double time, double U, double R)
    {
        double i;
        i = (U-accumU(time, U, R))/R;
        return i;
    }

    public double calcU(double Uin, double I,double time) {
        double Uout;
        Uout = Uin + I/C*time;
        return Uout;
    }

    public void calcUout(double I,double time) {
        Uout +=  I/C*time;
    }

    public void calcUin(double I,double time) {
        Uin += + I/C*time;
    }
}
