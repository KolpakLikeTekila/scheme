package sample;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import sample.DataCharacters.ChangeCharacters;
import sample.DataCharacters.Characters;
import sample.Elements.Capacitor;
import sample.Scheme;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Колпаков Антон on 04.07.2016.
 */
public class ControllerValue {
    @FXML
    public LineChart LineChartU;
    public LineChart LineChartI;
    public TextArea txtOut;

    private Stage dialogStage;
    private double timeStartSim, timeEndSim;
    ArrayList Scheme;
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setElements(ArrayList Scheme, double timeStartSim, double timeEndSim){
        this.Scheme = Scheme;
        this.timeStartSim = timeStartSim;
        this.timeEndSim= timeEndSim;
        PaintLineChart();
    }

    public void PaintLineChart() {
        LineChartU.getData().clear();
        LineChartI.getData().clear();
        Scheme sh = new Scheme(Scheme, timeStartSim, timeEndSim);
                     sh.Simulation();
                     Capacitor cap=null;
        sh.Simulation3();

     /*   XYChart.Series<Double, Double> seriesU = new XYChart.Series<Double, Double>();
        seriesU.setName("Напряжение");
        XYChart.Series<Double, Double> seriesI = new XYChart.Series<Double, Double>();
        seriesI.setName("Сила тока");
        for (Object obj:sh.GetListElements())
        {
            if(obj instanceof Capacitor)
            {
                cap = (Capacitor)obj;
            }
        }
        for (int i = 0; i < cap.GetCharacters().size(); i++) {
            Characters charact = cap.GetCharacters().get(i);
            seriesI.getData().add(new XYChart.Data<>(charact.GetmilSec(), charact.GetI()));
            seriesU.getData().add(new XYChart.Data<>(charact.GetmilSec(), charact.GetU()));
        }
        txtOut.appendText(GetStringChengeCharact(cap.GetListChangeU()) + GetStringChengeCharact(cap.GetListChangeI()));
        txtOut.setWrapText(true);
        Scheme sh2 = new Scheme(Scheme, timeStartSim, timeEndSim);
        sh2.Simulation2();
        XYChart.Series<Double, Double> seriesU2 = new XYChart.Series<Double, Double>();
        seriesU2.setName("Напряжение2");
        XYChart.Series<Double, Double> seriesI2 = new XYChart.Series<Double, Double>();
        seriesI2.setName("Сила тока2");
        for (Object obj:sh.GetListElements())
        {
            if(obj instanceof Capacitor)
            {
                cap = (Capacitor)obj;
            }
        }
        for (int i = 0; i < cap.GetCharacters().size(); i++) {
            Characters charact = cap.GetCharacters().get(i);
            seriesI2.getData().add(new XYChart.Data<>(charact.GetmilSec(), charact.GetI()));
            seriesU2.getData().add(new XYChart.Data<>(charact.GetmilSec(), charact.GetU()));
        }

        LineChartI.getData().addAll(seriesI,seriesI2);
        LineChartU.getData().addAll(seriesU,seriesU2);
*/
    }

    public String GetStringChengeCharact(ArrayList<ChangeCharacters> obj)
    {
        String str = "";
        String charactName ="";
        double timeCharactStartChange = 0;
        switch (obj.get(obj.size()-1).GetCharName()){
            case "U":
                charactName = "напряжения: ";
                break;
            case "I":
                charactName = "силы тока: ";
                break;
        }

        for (ChangeCharacters charact : obj) {
            if (charact.GetUp() == true && charact.GetStab() ==false){
                timeCharactStartChange = charact.GetmilSec();
                str += "Время начала роста " + charactName + charact.GetmilSec() + "; Значение : " + charact.GetChar() + "\n";
            }
            else if (charact.GetUp() == false && charact.GetStab() ==false){
                timeCharactStartChange = charact.GetmilSec();
                str += "Время начала падения " + charactName + charact.GetmilSec() + "; Значение : " + charact.GetChar() + "\n";
            }
            else {
                str += "Время стабилизации изменения " + charactName + timeCharactStartChange + "\n";
                str += "Время окончания изменения " + charactName + (charact.GetmilSec() -timeCharactStartChange) + "\n";
            }
        }
        return str;
    }
}
