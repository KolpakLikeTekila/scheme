package sample;

/**
 * Created by Колпаков Антон on 24.06.2016.
 */
public class Time {
    public double TimeStart;
    public double TimeEnd;
    static final int MILISEC = 1000;

    public Time (double _TimeStart, double _TimeEnd){
        TimeStart=_TimeStart;
        TimeEnd=_TimeEnd;
    }

    public double GetTimeStart()
    {
        return TimeStart;
    }

    public double GetTimeEnd()
    {
        return TimeEnd;
    }

    public double GetNumberStep()
    {
        return (TimeEnd-TimeStart)*MILISEC;
    }

    static double ConvertToMilSec(int i){
        return (double)i/MILISEC;
    }
    static int ConvertFromMilSec(double i){
        return (int)i*MILISEC;
    }

    public static double ConvertToMilSec(double i){
        return i/MILISEC;
    }
}
