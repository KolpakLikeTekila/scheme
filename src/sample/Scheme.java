package sample;

import sample.Elements.Capacitor;
import sample.Elements.EMF;
import sample.Elements.Resistor;

import java.util.ArrayList;

/**
 * Created by Колпаков Антон on 24.06.2016.
 */
public class Scheme {
    ArrayList<ArrayList> Elements;
    Time time;
    public double I;

    double E;
    double R;
    final double ERROR=0.0001;
    boolean UpU = false, DownU = false;
    boolean DownI = false;


    public Scheme(ArrayList _Elements, double TimeStartSim, double TimeEndSim){
        for (Object Element : _Elements) {
            if (Element instanceof Capacitor) {
               Capacitor data = (Capacitor) Element;
                data.clearData();
            }
            else if (Element instanceof Resistor) {
                Resistor data = (Resistor) Element;
                data.clearData();
            }
        }
        Elements=_Elements;
        time=new Time(TimeStartSim, TimeEndSim);
    }

    public ArrayList GetListElements(){
        return Elements;
    }

    public void CalcI(){
        Resistor res;
        EMF emf;
        double r = 0;
        for (Object Element : Elements) {
            if (Element instanceof EMF)
            {
                emf = (EMF) Element;
                E+=emf.getE();
                R+=emf.getR();
                r+=emf.getR();
            }
            else if(Element instanceof Resistor)
            {
                res = (Resistor) Element;
                R += res.getR();
            }
        }
        I = E/R;
        R-=r;
    }

    public double LineVolage(double time) {
        double E=0;
        EMF emf;
        for (Object Element : Elements) {
            if (Element instanceof EMF) {
                emf = (EMF) Element;
                if (emf.getTimeConnect() <= time && emf.getTimeWork() + emf.getTimeConnect() >= time) {
                    E += emf.getE();
                }
            }
        }
        return E;
    }

    public void CalcCharR(Resistor res){
        /*      res = (Resistor) Element;
                    double u = res.CalcU(U,I);
                    res.AddCharact(Math.abs(U-u),Math.abs(U-u)/res.GetR(),timeWork);
                    U -= u;*/
    }
    public  void CalcCharC(Capacitor cap, double EOld, double timeWork, double timeVoltChange){
        double u = 0;
        double i = 0;

        if (EOld < E) {
            u = EOld + cap.accumU(timeWork - timeVoltChange, E - EOld, R);
            i = cap.accumI(timeWork - timeVoltChange, E - EOld, R);
            if (timeWork == timeVoltChange) {
                cap.addChangeU(u, true, false, timeWork);
                cap.addChangeI(i, false, false, timeWork);
            }
            if (Math.abs(E - u) >= ERROR && Math.abs(E - EOld - cap.accumU(timeWork - timeVoltChange + Time.ConvertToMilSec(1), E - EOld, R)) < ERROR) {
                cap.addChangeU(u, true, true, timeWork);
            }
            if (Math.abs(i - 0) > ERROR&& Math.abs(cap.accumI(timeWork - timeVoltChange + Time.ConvertToMilSec(1), E, R) - 0) <= ERROR) {
                cap.addChangeI(i, false, true, timeWork);
            }
        }
        if (EOld > E) {
            u = E + cap.disU(timeWork - timeVoltChange, EOld - E, R);
            i = cap.disI(timeWork - timeVoltChange, EOld - E, R);
            if (timeWork == timeVoltChange) {
                cap.addChangeU(u,false, false, timeWork);
                cap.addChangeI(i,false, false, timeWork);
            }
            if (Math.abs(E - u) > ERROR && Math.abs(cap.disU(timeWork - timeVoltChange + Time.ConvertToMilSec(1), EOld - E, R)) <= ERROR) {
                cap.addChangeU(u, true, true, timeWork);
            }
            if (Math.abs(i - 0) > ERROR&& Math.abs(cap.disI(timeWork - timeVoltChange + Time.ConvertToMilSec(1), EOld, R) - 0) <= ERROR) {
                cap.addChangeI(i, false, true, timeWork);
            }
        }

        cap.addCharact(u, i, timeWork);
    }

    public class AmperSection{
        double I;
        double R;
        ArrayList elements = new ArrayList();

       public AmperSection(ArrayList obj){
           I=0;
           R=0;
           elements = new ArrayList(obj);
       }

        public void addElement(Object obj){
            elements.add(obj);
        }
        public double getI(){
            return I;
        }
        public ArrayList getElements(){
            return elements;
        }

        public boolean equal(ArrayList elements2){
            if (elements.size()==elements2.size())
            {
                for(int i =0; i< elements.size();i++){
                    boolean cheak = false;
                    for(int j =0; j< elements2.size();j++){
                        if (elements.get(i) == elements2.get(j)){
                            cheak = true;
                        }
                    }
                    if(cheak!=true){
                        return  false;
                    }
                }
                return  true;
            }
            else return false;
        }
    }

    public void Simulation3() {
        double UinLine = 0;

        ArrayList<AmperSection> branch = new ArrayList<>();
        ArrayList<Branch> branchs = new ArrayList<>();
        AmperSection section;
        int numberNodes = 0;
        ArrayList<ArrayList> nodes = new ArrayList<>();
        getSection(Elements, branch, branchs);
        createBranch(branchs);

    }

    public void createBranch( ArrayList<Branch> branchs) {
        ArrayList<ArrayList> nodes = new ArrayList<>();
        ArrayList<Branch> branchsNew = new ArrayList<>();
        for (int i = 0; i < branchs.size() - 1; i++) {
            Branch branch= bridgeBranch(branchs.get(i), branchs.get(i + 1));
            if (branch!=null) {
                branchsNew.add(branch);
            }
            createNodes(branchs.get(i), branchs.get(i + 1),nodes);
        }
        while (branchsNew.size() != 0) {
            ArrayList<Branch> branchsNew2 = new ArrayList<>(branchsNew);
            branchsNew.clear();
            for (int i = 0; i < branchs.size(); i++) {
                for (int j = 0; i < branchsNew2.size(); i++) {
                    Branch branch= bridgeBranch(branchs.get(i), branchsNew2.get(j));
                    if (branch!=null) {
                        branchsNew.add(branch);
                    }
                }
            }
            branchs.addAll(branchsNew2);
        }
    }

    public  void createNodes( Branch branch1, Branch branch2, ArrayList<ArrayList> nodes) {
        ArrayList<Node> node = new ArrayList<>();
        Node sectionNode;
        AmperSection section;

        for (int i = 0; i < branch1.sizeBranch(); i++) {
            for (int j = 0; j < branch2.sizeBranch(); j++) {
                if (branch1.getBranchOfIndex(i).equal(branch2.getBranchOfIndex(j).getElements())) {

                    section = branch2.getBranchOfIndex(j - 1);
                    sectionNode = new Node(true, section);
                    node.add(sectionNode);

                    section = branch1.getBranchOfIndex(i + 1);
                    sectionNode = new Node(false, section);
                    node.add(sectionNode);

                    section = branch1.getBranchOfIndex(i);
                    sectionNode = new Node(false, section);
                    node.add(sectionNode);

                    nodes.add((ArrayList)node.clone());
                    node.clear();

                    section = branch2.getBranchOfIndex(j);
                    sectionNode = new Node(false, section);
                    node.add(sectionNode);

                    section = branch1.getBranchOfIndex(i + 1);
                    sectionNode = new Node(true, section);
                    node.add(sectionNode);

                    section = branch1.getBranchOfIndex(i);
                    sectionNode = new Node(true, section);
                    node.add(sectionNode);

                    nodes.add(node);

                    break;
                }
            }
        }
    }

    public class Branch {
        ArrayList<Branch> branchList = new ArrayList<>();
        ArrayList<AmperSection> branch = new ArrayList<>();

        public Branch(ArrayList<AmperSection> _branch, ArrayList _branchList) {
            branch = _branch;
            branchList = _branchList;
        }

        public ArrayList<AmperSection> getBranch() {
            return branch;
        }

        public ArrayList<Branch> getBranchList() {
            return branchList;
        }

        public AmperSection getBranchOfIndex(int index) {
            return branch.get(index);
        }

        public int sizeBranch() {
            return branch.size();
        }

        public boolean isAncestor(ArrayList<Branch> _branch) {
            for (Branch ancBranch : _branch) {
                ArrayList<AmperSection> br = ancBranch.getBranch();
                int check = 0;
                for (int i = 0; i < br.size(); i++) {
                    for (int j = 0; j < branch.size(); j++) {
                        if (br.get(i).equal(branch.get(j).getElements())) {
                            check++;
                        }
                    }
                }
                if (check == _branch.size()) {
                    return true;
                }
            }
            return false;
        }
    }

    public Branch bridgeBranch( Branch branch1, Branch branch2) {
        ArrayList<AmperSection> branch = new ArrayList<>();
        AmperSection section;
        Branch branchClass = null;
        if (!(branch1.isAncestor(branch2.getBranchList())||branch2.isAncestor(branch1.getBranchList()))) {
            for (int i = 0; i < branch1.sizeBranch(); i++) {
                for (int j = 0; j < branch2.sizeBranch(); j++) {
                    if (branch1.getBranchOfIndex(i).equal(branch2.getBranchOfIndex(j).getElements())) {
                        for (int k = 0; k < i; k++) {
                            section = branch1.getBranchOfIndex(k);
                            branch.add(section);
                        }
                        for (int k = 0; k < j; k++) {
                            section = branch2.getBranchOfIndex(k);
                            branch.add(section);
                        }
                        for (int k = j + 1; k < branch2.sizeBranch(); k++) {
                            section = branch2.getBranchOfIndex(k);
                            branch.add(section);
                        }
                        for (int k = i + 1; k < branch1.sizeBranch(); k++) {
                            section = branch1.getBranchOfIndex(k);
                            branch.add(section);
                        }
                        ArrayList ancestorBranch = new ArrayList();
                        ancestorBranch.add(branch1);
                        ancestorBranch.add(branch2);
                        branchClass = new Branch(branch, ancestorBranch);
                        break;
                    }
                }
            }
        }
        return branchClass;
    }

    public class Node{
        boolean directClock;
        AmperSection section;
        public Node(boolean _directClock,AmperSection _section){
            directClock= _directClock;
            section = _section;
        }
    }

    public void  getSection(ArrayList<ArrayList> elements,  ArrayList<AmperSection> branch, ArrayList<Branch> branchs){
        AmperSection section;
        section = new AmperSection( elements.get(0));
        branch.add(section);
        if (elements.size()!=1) {
            for (int i = 1; i < elements.size(); i++) {
                if (elements.get(i).size()>1 ) {
               //     for (int j = 0; j<elements.get(i).size();j++){
                        section = new AmperSection( elements.get(i));
                        branch.add(section);

                        ArrayList<AmperSection> branchNext = new ArrayList<>();
                  //      if(((ArrayList) elements.get(i).get(j)).size()!=0) {
                            getSection(elements.get(i), branchNext, branchs);
                   //     }
                  //  }
                } else {
                    section = new AmperSection(elements.get(i));
                    branch.add(section);
                }
            }
        }
        Branch br = new Branch(branch, new ArrayList());
        branchs.add(br);
    }

    public void Simulation2() {
        double EOld = 0;
        //CalcI();
        int timeConnect = 0;
        while (E != LineVolage(Time.ConvertToMilSec(timeConnect))) {
            timeConnect++;
        }

        double timeVoltChange = 0;
        double ULine = 0, ILine = 0;
        double Uin = 0, Uout = 0;
        double u = 0;
        boolean start = true;
        int nuberCircl = 1;
        double timeWork = Time.ConvertToMilSec(1);

  /*      for (int i = 0; i < time.GetNumberStep(); i++) {
            E = LineVolage(Time.ConvertToMilSec(i + 1) + time.GetTimeStart());
            for(int j =0; j< Elements.size();j++){
                if (Elements.get(j) instanceof Resistor) {
                    Resistor res = (Resistor) Elements.get(j);
                    res.setUin(E);
                    res.setUout(u);
                    ILine = res.CalcI();
                } else if (Elements.get(j) instanceof Capacitor) {
                    Capacitor cap = (Capacitor) Elements.get(j);
                    u+=cap.calcU(0,ILine,timeWork);
                }
            }
            for(int j =0; j< Elements.size();j++){
                if (Elements.get(j) instanceof Capacitor) {
                    Capacitor cap = (Capacitor) Elements.get(j);
                    cap.addCharact(u, ILine, Time.ConvertToMilSec(i + timeConnect));}
            }
        }*/
    }

    public void Simulation() {
        double timeWork;
        Resistor res;
        double EOld = 0;
        double timeVoltChange = 0;
        //     CalcI();
        E = LineVolage(time.GetTimeStart());
        if (E != EOld) {
            timeVoltChange = time.GetTimeStart();
        }
        for (Object Element : Elements) {
            if (Element instanceof Resistor) {
                res = (Resistor) Element;
                R += res.getR();
            }
        }
        for (int i = 0; i < time.GetNumberStep(); i++) {
            timeWork = Time.ConvertToMilSec(i) + time.GetTimeStart();
            for (Object Element : Elements) {
                if (Element instanceof Capacitor) {
                    CalcCharC((Capacitor) Element, EOld, timeWork, timeVoltChange);
                }
            }
            if (E != LineVolage(Time.ConvertToMilSec(i + 1) + time.GetTimeStart())) {
                EOld = E;
                E = LineVolage(Time.ConvertToMilSec(i + 1) + time.GetTimeStart());
                timeVoltChange = Time.ConvertToMilSec(i + 1);
            }
        }
    }

  }
