package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import sample.DataCharacters.PosInGrid;
import sample.Elements.Capacitor;
import sample.Elements.Circuit;
import sample.Elements.EMF;
import sample.Elements.Resistor;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Created by Колпаков Антон on 01.07.2016.
 */
public class ControllerCharacters implements Initializable {

    @FXML
    public GridPane grid2;
    public Button btnAdd;
    public TextField txtLabel;
    public TextField txtEName;
    public TextField txtEr;
    public TextField txtE;
    public TextField txtTWork;
    public TextField txtTConnect;
    public TextField txtC;
    public TextField txtCName;
    public TextField txtR;
    public TextField txtRName;
    public TextField txtEr1;
    public TextField txtE1;
    public TextField txtTWork1;
    public TextField txtTConnect1;
    public TextField txtName;
    public Label lblR;
    public Label lblE;
    public Label lblTcon;
    public Label lblTWork;
    public Button btnDelet;
    public Button btnRewrite;
    public Label lblName;
    public Button btnI;
    public Button btnX;
    public Button btnT;
    public Button btnCountClockwise;
    public Button btnL;
    public Button btnClockwise;
    public ImageView imgBig;
    public TextField txtTiemStartSim;
    public TextField txtTiemEndSim;
    @FXML
    GridPane Grid;
    PosInGrid activCell = new PosInGrid(-1, -1);
    PosInGrid firstElInCirc = new PosInGrid(-1, -1);

    final String TYPE_IMG_L = "L";
    final String TYPE_IMG_X = "X";
    final String TYPE_IMG_I = "I";
    final String TYPE_IMG_T = "T";
    final String TYPE_ELEMENT = "ELEMENT";
    final int GRID_SIZE_CELL_WIDTH = 9;
    final int GRID_SIZE_CELL_HEIGHT = 9;
    ArrayList Scheme = new ArrayList();

    final String PATCH_TO_IMAGE_DIRECT = "file:\\D:\\work\\scheme\\resources\\Image\\";

    private class SchemeCell {
        ArrayList<PosInGrid> nextPos = new ArrayList<>();
        Object element = null;

        SchemeCell() {

        }

        ArrayList<PosInGrid> getNextPos() {
            return nextPos;
        }

        void setNextPos(ArrayList<PosInGrid> _nextPos) {
            nextPos = _nextPos;
        }

        Object getElement() {
            return element;
        }

        void setElement(Object _element) {
            element = _element;
        }

    }

    private class ArrayListSerialElements {
        ArrayList elements = new ArrayList();
        ;

        ArrayList get() {
            return elements;
        }

        ArrayList get(int index) {
            return (ArrayList) elements.get(index);
        }

        int size() {
            return elements.size();
        }

        void add(Objects obj) {
            elements.add(obj);
        }
    }

    public SchemeCell getNextPatch(PosInGrid pos) {
        GridCell gr = (GridCell) getNodeFromGridPane(Grid, pos.getPosCol(), pos.getPosRow());
        SchemeCell sc = new SchemeCell();
        if (gr.getType() == TYPE_ELEMENT) {
            sc.setElement(gr.getElement());
        }
        Circuit cir = (Circuit) gr.getCircuit();
        cir.buildOutput();
        ArrayList<PosInGrid> outputList = cir.getListOutput();
        for (PosInGrid line : outputList) {
            gr = (GridCell) getNodeFromGridPane(Grid, line.getPosCol(), line.getPosRow());
            ((Circuit) gr.getCircuit()).setInput(pos.getPosCol(), pos.getPosRow());
        }
        sc.setNextPos(outputList);
        return sc;
    }

    public void createCircle(ActionEvent actionEvent) {
        Scheme.clear();
        if (SeekFirstElemCircl()) {
            PosInGrid posEndSerial = getPosStartCross(firstElInCirc.clone());
            GridCell gr = (GridCell) getNodeFromGridPane(Grid, posEndSerial.getPosCol(), posEndSerial.getPosRow());
            PosInGrid startPos = ((Circuit) gr.getCircuit()).getInput();
            ((Circuit) gr.getCircuit()).setInput(posEndSerial.getPosCol(), posEndSerial.getPosRow());
            gr = (GridCell) getNodeFromGridPane(Grid, startPos.getPosCol(), startPos.getPosRow());
            ((Circuit) gr.getCircuit()).setInput(posEndSerial.getPosCol(), posEndSerial.getPosRow());
            ArrayList<ArrayList> elements = new ArrayList<>();
            ArrayList<PosInGrid> endPos = new ArrayList<>();
            endPos.add(startPos);
            posEndSerial = detourCircuit(startPos, endPos, elements);
            Scheme = elements;
               openScene();
        }
    }

    public PosInGrid detourCircuit(PosInGrid startPos,  ArrayList<PosInGrid> endPos, ArrayList elementsList) {
        ArrayList elementsListSerial = new ArrayList();
        PosInGrid posStartInputPos = startPos;
        PosInGrid posSerial = serialCircuit(posStartInputPos, endPos, elementsListSerial);
        elementsList.add(elementsListSerial.clone());
        elementsListSerial.clear();
        while (!(posSerial.equals(endPos))) {
            ArrayList<ArrayList> elementsListParal = new ArrayList<>();
            ArrayList <PosInGrid> posParal = paralCircuit(posSerial, endPos, elementsListParal);
            elementsList.add(elementsListParal.clone());
            elementsListParal.clear();
            for(PosInGrid pos:posParal) {
                posSerial = pos;
                posStartInputPos = pos;
                if (!(posSerial.equals(endPos))) {
                    posSerial = serialCircuit(posSerial, endPos, elementsListSerial);
                    elementsList.add(elementsListSerial.clone());
                    elementsListSerial.clear();
                }
                else{
                    break;
                }
            }
        }
        return posStartInputPos;
    }

    public ArrayList<PosInGrid> paralCircuit(PosInGrid posParal,  ArrayList<PosInGrid> endPos, ArrayList elementsList) {
        ArrayList elementsListSerial = new ArrayList();
        ArrayList<ArrayList> OutputsList = new ArrayList<>();
        SchemeCell sc = getNextPatch(posParal);

        ArrayList<PosInGrid> posSerial = sc.getNextPos();
        PosInGrid posCros = serialCircuit(posSerial.get(0), endPos, elementsListSerial);
        PosInGrid posStartInputPos;
        elementsList.add(elementsListSerial.clone());
        elementsListSerial.clear();

        ArrayList<PosInGrid> outputs = (ArrayList<PosInGrid>) getNextPatch(posCros).getNextPos().clone();
        OutputsList.add(outputs);
        for (int i = 1; i < posSerial.size(); i++) {
            ArrayList<ArrayList> elementsListParal = new ArrayList();
            ArrayList<PosInGrid> branch = new ArrayList<>(endPos);
            branch.add(posCros);
            posStartInputPos = detourCircuit(posSerial.get(i), branch, elementsListParal);
            elementsList.add(elementsListParal.clone());
            elementsListParal.clear();

           // PosInGrid posCros2 = serialCircuit(posSerial.get(i), endPos, elementsListSerial);
          //  elementsListSerial.clear();
          //  if(posCros.equals(posCros2)){
                outputs = (ArrayList<PosInGrid>) getNextPatch(serialCircuit(posStartInputPos, endPos, elementsListSerial)).getNextPos().clone();
                OutputsList.add(outputs);
          //  }
        }
        return outputCalc(OutputsList);
    }

    private ArrayList<PosInGrid> outputCalc(ArrayList<ArrayList> list){
        ArrayList<PosInGrid> list1 = list.get(0);
        ArrayList<PosInGrid> list3 = new ArrayList<>();
        for(int i = 1; i< list.size();i++)
        {
            ArrayList<PosInGrid> list2 = list.get(i);
            for(int j=0;j<list1.size();j++)
                for(int k =0; k<list2.size();k++){
                    if(list1.get(j).equals(list2.get(k))){
                        list3.add(list2.get(k));
                    }
                }
            list1 = new ArrayList<>(list3);
            list3.clear();
        }
        return list1;
    }

    public PosInGrid serialCircuit(PosInGrid startPos,  ArrayList<PosInGrid> endPos, ArrayList elementsList) {
        SchemeCell sc = getNextPatch(startPos);
        PosInGrid pos=startPos;
        ArrayList<PosInGrid> posSerial = sc.getNextPos();
        while (posSerial.size() == 1) {
            if (sc.getElement() != null) {
                elementsList.add(sc.getElement());
            }
            pos = posSerial.get(0);
            sc = getNextPatch(posSerial.get(0));
            posSerial = sc.getNextPos();
            if (pos.equals(endPos)) {
                break;
            }
        }
        return pos;
    }

    public PosInGrid getPosStartCross(PosInGrid pos) {
        PosInGrid pos2;
        GridCell gr = (GridCell) getNodeFromGridPane(Grid, pos.getPosCol(), pos.getPosRow());
        ((Circuit) gr.getCircuit()).setInput(pos.getPosCol() + 1, pos.getPosRow());
        pos2 = getNextPatch(pos).getNextPos().get(0);
        ((Circuit) gr.getCircuit()).setInput(pos.getPosCol(), pos.getPosRow());
        while ((pos2.getPosCol() != pos.getPosCol() || pos2.getPosRow() != pos.getPosRow()) && gr.getType() != TYPE_IMG_T && gr.getType() != TYPE_IMG_X) {
            PosInGrid pos3 = pos2;
            pos2 = getNextPatch(pos2).getNextPos().get(0);

            gr = (GridCell) getNodeFromGridPane(Grid, pos3.getPosCol(), pos3.getPosRow());
            ((Circuit) gr.getCircuit()).setInput(pos3.getPosCol(), pos3.getPosRow());

            gr = (GridCell) getNodeFromGridPane(Grid, pos2.getPosCol(), pos2.getPosRow());
        }
        return pos2;
    }

    public void openScene() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Value.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Сила и напряжение тока");
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ControllerValue controller;
            controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setElements(Scheme, Double.parseDouble(txtTiemStartSim.getText()), Double.parseDouble(txtTiemEndSim.getText()));

            dialogStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean SeekFirstElemCircl() {
        for (int i = 0; i < GRID_SIZE_CELL_WIDTH; i++) {
            for (int j = 0; j < GRID_SIZE_CELL_HEIGHT; j++) {
                GridCell gr = (GridCell) getNodeFromGridPane(Grid, i, j);
                if (gr.getActive()) {
                    firstElInCirc = new PosInGrid(i, j);
                    return true;
                }
            }
        }
        return false;
    }

    public class GridCell extends StackPane {
        Label label;
        Rectangle rect;
        Object element, circuit;
        ImageView img;
        String type;
        boolean active = false;
        int RECT_HEIGHT = 30;
        int RECT_WIDTH = 30;
        int row, col;

        public GridCell(int _col, int _row) {
            super();
            row = _row;
            col = _col;
            label = new Label();
            img = new ImageView();
            setAlignment(Pos.CENTER);
            rect = new Rectangle(0, 0, RECT_WIDTH, RECT_HEIGHT);
            rect.setFill(Color.TRANSPARENT);
            rect.setStroke(Color.BLACK);
            rect.setStrokeWidth(1);
            rect.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                       @Override
                                       public void handle(MouseEvent arg0) {
                                           //  Rectangle r= (Rectangle)arg0.getSource();
                                           visibleInfo(false);
                                           visibleTxt(false);
                                           clearBorderRect();
                                           rect.setStrokeWidth(3);
                                           rect.setStroke(Color.RED);
                                           activCell.setVal(col, row);
                                           getValElem();
                                       }
                                   }
            );

            getChildren().addAll(img, label, rect);
        }

        public void clearBorderRect() {
            ObservableList<Node> childrens = Grid.getChildren();
            for (Node node : childrens) {
                GridCell gr = (GridCell) node;
                gr.focusOutRect();
            }
        }

        public void setImage(String pathImg, String _type) {
            type = _type;
            Image imageDecline = new Image(PATCH_TO_IMAGE_DIRECT + pathImg);
            img.setImage(imageDecline);
            img.setFitWidth(30);
            img.setFitHeight(30);
        }

        public String getType() {
            return type;
        }

        public void setType(String Type) {
            type = Type;
        }

        public void clearImg() {
            img.setImage(null);
        }

        public void setRotateImage(double degree) {
            double rotate = img.getRotate();
            img.setRotate(rotate + degree);
        }

        public double getRotateImage() {
            return  img.getRotate();
        }

        public void activeTrue() {
            active = true;
        }

        public void activeFalse() {
            active = false;
        }

        public boolean getActive() {
            return active;
        }

        public void setLabel(String txt) {
            label.setText(txt);
        }

        public Object getElement() {
            return element;
        }

        public Object getCircuit() {
            return circuit;
        }

        public void setElement(Object _obj) {
            element = _obj;
        }

        public void setCircuit(Object _obj) {
            circuit = _obj;
        }

        public void focusOutRect() {
            rect.setStrokeWidth(1);
            rect.setStroke(Color.BLACK);

        }

    }

    private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
                return node;
            }
        }
        return null;
    }

    public void addEElement(ActionEvent actionEvent) {
        if (activCell.getPosCol() != -1 && activCell.getPosRow() != -1) {
            createE((GridCell) getNodeFromGridPane(Grid, activCell.getPosCol(), activCell.getPosRow()),
                    Double.parseDouble(txtE.getText()),
                    Double.parseDouble(txtEr.getText()),
                    Double.parseDouble(txtTConnect.getText()),
                    Double.parseDouble(txtTWork.getText()), txtEName.getText());
        }
    }

    public void createE(GridCell gr, double e, double r, double tCon, double tWork, String name) {
        gr.setLabel(name);
        EMF obj = new EMF(e, r, tCon, tWork, name);
        gr.setType(TYPE_ELEMENT);
        gr.setElement(obj);
        gr.activeTrue();
        gr.clearImg();
        gr.setCircuit(new Circuit(activCell.getPosCol(), activCell.getPosRow(), 0, TYPE_ELEMENT));
    }

    public void addCElement(ActionEvent actionEvent) {
        if (activCell.getPosCol() != -1 && activCell.getPosRow() != -1) {
            createC((GridCell) getNodeFromGridPane(Grid, activCell.getPosCol(), activCell.getPosRow()),
                    Double.parseDouble(txtC.getText()),
                    txtCName.getText());
        }
    }

    public void createC(GridCell gr, double c, String name) {
        gr.setLabel(name);
        Capacitor obj = new Capacitor(c, name);
        gr.setElement((Object) obj);
        gr.setType(TYPE_ELEMENT);
        gr.activeTrue();
        gr.clearImg();
        gr.setCircuit(new Circuit(activCell.getPosCol(), activCell.getPosRow(), 0, TYPE_ELEMENT));
    }

    public void addRElement(ActionEvent actionEvent) {
        if (activCell.getPosCol() != -1 && activCell.getPosRow() != -1) {
            createR((GridCell) getNodeFromGridPane(Grid, activCell.getPosCol(), activCell.getPosRow()),
                    Double.parseDouble(txtR.getText()),
                    txtRName.getText());
        }
    }

    public void createR(GridCell gr, double r, String name) {
        gr.setLabel(name);
        Resistor obj = new Resistor(r, name);
        Object Element = (Object) obj;
        gr.setElement(Element);
        gr.setType(TYPE_ELEMENT);
        gr.activeTrue();
        gr.clearImg();
        gr.setCircuit(new Circuit(activCell.getPosCol(), activCell.getPosRow(), 0, TYPE_ELEMENT));
    }

    public void deletlElem(ActionEvent actionEvent) {
        if (activCell.getPosCol() != -1 && activCell.getPosRow() != -1) {
            GridCell gr = (GridCell) getNodeFromGridPane(Grid, activCell.getPosCol(), activCell.getPosRow());
            gr.setLabel("");
            gr.setElement(null);
            gr.clearImg();
            gr.activeFalse();
        }
    }

    public void rewriteEle(ActionEvent actionEvent) {
        if (activCell.getPosCol() != -1 && activCell.getPosRow() != -1) {
            GridCell gr = (GridCell) getNodeFromGridPane(Grid, activCell.getPosCol(), activCell.getPosRow());
            Object Element = gr.getElement();
            if (Element != null) {
                if (Element instanceof Capacitor) {
                    createC(gr,
                            Double.parseDouble(txtEr1.getText()),
                            txtName.getText());

                } else if (Element instanceof Resistor) {
                    createR(gr,
                            Double.parseDouble(txtEr1.getText()),
                            txtName.getText());
                } else if (Element instanceof EMF) {
                    createE(gr,
                            Double.parseDouble(txtE1.getText()),
                            Double.parseDouble(txtEr1.getText()),
                            Double.parseDouble(txtTConnect1.getText()),
                            Double.parseDouble(txtTWork1.getText()),
                            txtName.getText());
                }

            }
        }
    }

    public void clearLine(ActionEvent actionEvent) {
        filGrid(Grid, GRID_SIZE_CELL_WIDTH, GRID_SIZE_CELL_HEIGHT);
    }


    public void visibleTxt(boolean check) {
        txtTConnect1.setVisible(check);
        txtTWork1.setVisible(check);
        txtE1.setVisible(check);
        lblTcon.setVisible(check);
        lblTWork.setVisible(check);
        lblE.setVisible(check);
    }

    public void visibleInfo(boolean check) {
        lblR.setVisible(check);
        lblName.setVisible(check);
        txtName.setVisible(check);
        txtEr1.setVisible(check);
    }


    public void getValElem() {
        GridCell gr = (GridCell) getNodeFromGridPane(Grid, activCell.getPosCol(), activCell.getPosRow());
        Object Element = gr.getElement();
        if (Element != null) {
            if (Element instanceof Capacitor) {
                visibleInfo(true);
                visibleTxt(false);

                Capacitor obj = (Capacitor) Element;
                txtEr1.setText(String.valueOf(obj.getC()));
                txtName.setText(obj.getNameElement());

            } else if (Element instanceof Resistor) {
                visibleInfo(true);
                visibleTxt(false);
                Resistor obj = (Resistor) Element;
                txtEr1.setText(String.valueOf(obj.getR()));
                txtName.setText(obj.getNameElement());
            } else if (Element instanceof EMF) {
                visibleInfo(true);
                visibleTxt(true);
                EMF obj = (EMF) Element;
                txtEr1.setText(String.valueOf(obj.getR()));
                txtE1.setText(String.valueOf(obj.getE()));
                txtTConnect1.setText(String.valueOf(obj.getTimeConnect()));
                txtTWork1.setText(String.valueOf(obj.getTimeWork()));
                txtName.setText(obj.getNameElement());
            }
        }
    }

    public void addLineCircl(String NameImg, String type) {
        if (activCell.getPosCol() != -1 && activCell.getPosRow() != -1) {
            GridCell gr = (GridCell) getNodeFromGridPane(Grid, activCell.getPosCol(), activCell.getPosRow());
            gr.setImage(NameImg, type);
            gr.activeTrue();
            gr.setLabel("");
            double rotate = gr.getRotateImage();
            gr.setRotateImage(-rotate);
            Circuit circ = new Circuit(activCell.getPosCol(), activCell.getPosRow(), 0, type);
            gr.setCircuit(circ);
        }
    }

    public void addI(ActionEvent actionEvent) {
        addLineCircl("палка.png", TYPE_IMG_I);
    }


    public void addX(ActionEvent actionEvent) {
        addLineCircl("х.png", TYPE_IMG_X);
    }

    public void addT(ActionEvent actionEvent) {
        addLineCircl("т.png", TYPE_IMG_T);
    }

    public void addL(ActionEvent actionEvent) {
        addLineCircl("г.png", TYPE_IMG_L);
    }

    public void rotate(double degree) {
        if (activCell.getPosCol() != -1 && activCell.getPosRow() != -1) {
            GridCell gr = (GridCell) getNodeFromGridPane(Grid, activCell.getPosCol(), activCell.getPosRow());
            gr.setLabel("");
            gr.setRotateImage(degree);
            ((Circuit) gr.getCircuit()).setRotate(degree);
        }
    }

    public void rotateClockwise(ActionEvent actionEvent) {
        rotate(90);
    }

    public void rotateCounterClockwise(ActionEvent actionEvent) {
        rotate(-90);
    }

    public void filGrid(GridPane grid, int row, int col) {
        grid.getChildren().clear();
        grid.setPadding(new Insets(5));
        grid.setHgap(2);
        grid.setVgap(2);
        grid.setAlignment(Pos.CENTER);

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                GridCell gr = new GridCell(i, j);
                grid.add(gr, i, j);
            }
        }
    }

    public void createBorderBut(int width, int height, String NameBorderFile, Button btn) {
        Image imageDecline = new Image(PATCH_TO_IMAGE_DIRECT + NameBorderFile);
        ImageView imageView = new ImageView(imageDecline);
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);
        btn.setGraphic(imageView);
        btn.setMaxSize(width, height);
    }

    public void buildScheme(ActionEvent actionEvent) {
       activCell.setVal(0, 2);
        addL(actionEvent);
        rotateCounterClockwise(actionEvent);
        activCell.setVal(1, 2);
        addEElement(actionEvent);
        activCell.setVal(2, 2);
        addI(actionEvent);
        activCell.setVal(3, 2);
        addEElement(actionEvent);
        activCell.setVal(4, 2);
        addI(actionEvent);
        activCell.setVal(5, 2);
        addEElement(actionEvent);
        activCell.setVal(6, 2);
        addI(actionEvent);
        activCell.setVal(7, 2);
        addL(actionEvent);

        activCell.setVal(0, 3);
        addL(actionEvent);
        rotateClockwise(actionEvent);
        rotateClockwise(actionEvent);
        activCell.setVal(1, 3);
        addT(actionEvent);
        activCell.setVal(2, 3);
        addI(actionEvent);
        activCell.setVal(3, 3);
        addRElement(actionEvent);
        activCell.setVal(4, 3);
        addI(actionEvent);
        activCell.setVal(5, 3);
        addRElement(actionEvent);
        activCell.setVal(6, 3);
        addT(actionEvent);
        activCell.setVal(7, 3);
        addL(actionEvent);
        rotateClockwise(actionEvent);

        activCell.setVal(1, 4);
        addL(actionEvent);
        rotateCounterClockwise(actionEvent);
        rotateCounterClockwise(actionEvent);
        activCell.setVal(2, 4);
        addI(actionEvent);
        activCell.setVal(3, 4);
        addCElement(actionEvent);
        activCell.setVal(4, 4);
        addI(actionEvent);
        activCell.setVal(5, 4);
        addI(actionEvent);
        activCell.setVal(6, 4);
        addL(actionEvent);
        rotateClockwise(actionEvent);


  /*      activCell.setVal(0, 2);
        addL(actionEvent);
        rotateCounterClockwise(actionEvent);
        activCell.setVal(1, 2);
        addEElement(actionEvent);
        activCell.setVal(2, 2);
        addI(actionEvent);
        activCell.setVal(3, 2);
        addI(actionEvent);
        activCell.setVal(4, 2);
        addI(actionEvent);
        activCell.setVal(5, 2);
        addI(actionEvent);
        activCell.setVal(6, 2);
        addL(actionEvent);

        activCell.setVal(0, 3);
        addL(actionEvent);
        rotateClockwise(actionEvent);
        rotateClockwise(actionEvent);
        activCell.setVal(1, 3);
        addI(actionEvent);
        activCell.setVal(2, 3);
        addRElement(actionEvent);
        activCell.setVal(3, 3);
        addI(actionEvent);
        activCell.setVal(4, 3);
        addCElement(actionEvent);
        activCell.setVal(5, 3);
        addI(actionEvent);
        activCell.setVal(6, 3);
        addL(actionEvent);
        rotateClockwise(actionEvent);*/

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            filGrid(Grid, GRID_SIZE_CELL_WIDTH, GRID_SIZE_CELL_HEIGHT);
            createBorderBut(30, 30, "х.png", btnX);
            createBorderBut(30, 30, "г.png", btnL);
            createBorderBut(30, 30, "т.png", btnT);
            createBorderBut(30, 30, "палка.png", btnI);
            createBorderBut(30, 30, "по.png", btnClockwise);
            createBorderBut(30, 30, "против.png", btnCountClockwise);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
