package sample.DataCharacters;

/**
 * Created by Колпаков Антон on 28.06.2016.
 */
public class ChangeCharacters {
    public double charact;
    public boolean up;
    public boolean stab;
    public double milSec;
    public String charactName;

    ChangeCharacters(double _Charact, boolean _up, boolean _stab, double _milSec, String _charactName) {
        charact = _Charact;
        up = _up;
        stab = _stab;
        milSec = _milSec;
        charactName = _charactName;
    }

    public double GetChar() {
        return charact;
    }

    public String GetCharName() {
        return charactName;
    }

    public boolean GetUp() {
        return up;
    }

    public boolean GetStab() {
        return stab;
    }

    public double GetmilSec() {
        return milSec;
    }
}
