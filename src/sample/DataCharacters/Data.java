package sample.DataCharacters;

import java.util.ArrayList;

/**
 * Created by Колпаков Антон on 24.06.2016.
 */
public abstract class Data {

    public ArrayList<ChangeCharacters> ListChangeU= new ArrayList();
    public ArrayList<ChangeCharacters> ListChangeI= new ArrayList();
    public ArrayList<Characters> ListCharacter = new ArrayList();
    public String NameElement;

    public void addCharact( double U, double I,double milSec)
    {
        Characters charact = new Characters(U,I,milSec);
        ListCharacter.add(charact);
    }
    public void addChangeI( double I, boolean up, boolean stab, double milSec)
    {
        ChangeCharacters i = new ChangeCharacters(I,up,stab,milSec,"I");
        ListChangeI.add(i);
    }
    public void addChangeU( double U, boolean up, boolean stab, double milSec)
    {
        ChangeCharacters u = new ChangeCharacters(U,up,stab,milSec,"U");
        ListChangeU.add(u);
    }
    public ArrayList<Characters> GetCharacters(){
        return ListCharacter;
    }
    public ArrayList<ChangeCharacters> GetListChangeI(){
        return ListChangeI;
    }
    public ArrayList<ChangeCharacters> GetListChangeU(){
        return ListChangeU;
    }
    public void SetNameElement(String name){
        NameElement=name;
    }
    public String getNameElement(){return NameElement;}

    public void clearData(){
        ListCharacter.clear();
        ListChangeI.clear();
        ListChangeU.clear();
    }

}
