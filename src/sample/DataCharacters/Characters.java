package sample.DataCharacters;

/**
 * Created by Колпаков Антон on 28.06.2016.
 */
    public class Characters {
    public double U;
    public double I;
    public double milSec;

    Characters(double _U, double _I, double _milSec) {
        U = _U;
        I = _I;
        milSec = _milSec;
    }

    public double GetU() {
        return U;
    }

    public double GetI() {
        return I;
    }

    public double GetmilSec() {
        return milSec;
    }
}
