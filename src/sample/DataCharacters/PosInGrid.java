package sample.DataCharacters;

import java.util.ArrayList;

/**
 * Created by Колпаков Антон on 03.07.2016.
 */
public class PosInGrid{
    int PosCol;
    int PosRow;
    public PosInGrid(int col, int row){
        PosCol = col;
        PosRow = row;
    }

    public PosInGrid clone() {
        PosInGrid emp;
        emp = new PosInGrid(this.PosCol, this.PosRow);
        return emp;
    }
    public int getPosCol(){
        return PosCol;
    }
    public int getPosRow(){
        return  PosRow;
    }
    public void setVal(int col, int row){
        PosCol = col;
        PosRow = row;
    }

    public boolean equals(ArrayList<PosInGrid> obj){
        for(PosInGrid pos: obj)
        if (PosCol==pos.getPosCol()&&PosRow==pos.getPosRow()) {
            return true;
        }
        return false;
    }

    public boolean equals(PosInGrid obj){
            if (PosCol==obj.getPosCol()&&PosRow==obj.getPosRow()) {
                return true;
            }
        return false;
    }
}
