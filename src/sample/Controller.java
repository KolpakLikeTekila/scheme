package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.DataCharacters.ChangeCharacters;
import sample.DataCharacters.Characters;
import sample.Elements.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    ArrayList Elements = new ArrayList();
    String str;
    @FXML
    TextArea txtOut;

    @FXML
    Button btnAddR;
    @FXML
    TextField txtR;

    public void AddRElement(ActionEvent actionEvent) {
     //   Resistor r = new Resistor(Double.parseDouble(txtR.getText()));
    //    Elements.add(r);
    //   writeScheme(r);
        txtOut.clear();
        txtOut.appendText(str + " - \n");
    }

    @FXML
    Button btnAddC;
    @FXML
    TextField txtC;

    public void AddCElement(ActionEvent actionEvent) {
    //    Capacitor c = new Capacitor(Double.parseDouble(txtC.getText()));
     //   Elements.add(c);
     //   writeScheme(c);
        txtOut.clear();
        txtOut.appendText(str + " - \n");
    }

    @FXML
    Button btnAddE;
    @FXML
    TextField txtE;
    @FXML
    TextField txtEr;
    @FXML
    TextField txtTConnect;
    @FXML
    TextField txtTWork;

    public void AddEElement(ActionEvent actionEvent) {
  //      EMF e = new EMF(Double.parseDouble(txtE.getText()), Double.parseDouble(txtEr.getText()), Double.parseDouble(txtTConnect.getText()), Double.parseDouble(txtTWork.getText()));

      //  Elements.add(e);
      //  writeScheme(e);
        txtOut.clear();
        txtOut.appendText(str + " - \n");
    }

    public void writeScheme(Object el){
        if (str==null){
            str = "+ ";
            str += "—";
        }
        if (el instanceof EMF){
            str += "E";
            str += "—";
        }
        if (el instanceof Capacitor){
            str += "C";
            str += "—";
        }
        if (el instanceof Resistor){
            str += "R";
            str += "—";
        }
    }


    @FXML
    LineChart<Double, Double> LineChartI;
    @FXML
    LineChart<Double, Double> LineChartU;
    @FXML
    TextField txtTiemStartSim;
    @FXML
    TextField txtTiemEndSim;

    public String GetStringChengeCharact(ArrayList<ChangeCharacters> obj)
    {
        String str = "";
        String charactName ="";
        double timeCharactStartChange = 0;
        switch (obj.get(obj.size()-1).GetCharName()){
            case "U":
                charactName = "напряжения: ";
                break;
            case "I":
                charactName = "силы тока: ";
                break;
        }

        for (ChangeCharacters charact : obj) {
            if (charact.GetUp() == true && charact.GetStab() ==false){
                timeCharactStartChange = charact.GetmilSec();
                str += "Время начала роста " + charactName + charact.GetmilSec() + "; Значение : " + charact.GetChar() + "\n";
            }
            else if (charact.GetUp() == false && charact.GetStab() ==false){
                timeCharactStartChange = charact.GetmilSec();
                str += "Время начала падения " + charactName + charact.GetmilSec() + "; Значение : " + charact.GetChar() + "\n";
            }
            else {
                str += "Время стабилизации изменения " + charactName + timeCharactStartChange + "\n";
                str += "Время окончания изменения " + charactName + (charact.GetmilSec() -timeCharactStartChange) + "\n";
            }
        }
        return str;
    }

/*
    public void PaintLineChart(ActionEvent e) {
        LineChartU.getData().clear();
        LineChartI.getData().clear();
        Scheme sh = new Scheme(Elements, Double.parseDouble(txtTiemStartSim.getText()), Double.parseDouble(txtTiemEndSim.getText()));
        sh.Simulation();


        XYChart.Series<Double, Double> seriesU = new XYChart.Series<Double, Double>();
        seriesU.setName("Напряжение");
        XYChart.Series<Double, Double> seriesI = new XYChart.Series<Double, Double>();
        seriesI.setName("Сила тока");
        Capacitor cap = (Capacitor) sh.GetListElements().get(2);
        for (int i = 0; i < cap.GetCharacters().size(); i++) {
            Characters charact = cap.GetCharacters().get(i);
            seriesI.getData().add(new XYChart.Data<>(charact.GetmilSec(), charact.GetI()));
            seriesU.getData().add(new XYChart.Data<>(charact.GetmilSec(), charact.GetU()));
        }
        Scheme sh2 = new Scheme(Elements, Double.parseDouble(txtTiemStartSim.getText()), Double.parseDouble(txtTiemEndSim.getText()));
       sh2.Simulation2();
        XYChart.Series<Double, Double> seriesU2 = new XYChart.Series<Double, Double>();
        seriesU2.setName("Напряжение2");
        XYChart.Series<Double, Double> seriesI2 = new XYChart.Series<Double, Double>();
        seriesI2.setName("Сила тока2");
        cap = (Capacitor) sh2.GetListElements().get(2);
        for (int i = 0; i < cap.GetCharacters().size(); i++) {
            Characters charact = cap.GetCharacters().get(i);
            seriesI2.getData().add(new XYChart.Data<>(charact.GetmilSec(), charact.GetI()));
            seriesU2.getData().add(new XYChart.Data<>(charact.GetmilSec(), charact.GetU()));
        }

        LineChartI.getData().addAll(seriesI,seriesI2);
        LineChartU.getData().addAll(seriesU,seriesU2);

        txtOut.appendText(GetStringChengeCharact(cap.GetListChangeU()) + GetStringChengeCharact(cap.GetListChangeI()));
    }
*/
    public void ClearTxtOut(ActionEvent actionEvent) {
        txtOut.clear();
        Elements.clear();
        str=null;
    }

    public void  showCharacter(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("Characters.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Hello World2");
            stage.setScene(new Scene(root));
            stage.show();

           /* FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Characters.fxml"));
            Parent page = (Parent)fxmlLoader.load();

            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle("ABC");
            stage.setScene(new Scene(page));
            stage.show();*/
        } catch(Exception e) {
            e.printStackTrace();
        }
    }





/*
    @FXML
    private RadioButton radioBtn1;
    @FXML
    private RadioButton radioBtn2;
*/

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
         /*  Image image;
            image = new Image(new FileInputStream("D:\\work\\scheme\\src\\sample\\1.png")); //на этой строчке вылетает - что не правильно?
            imgPic.setImage(image);
            final ToggleGroup radiobtnGroup = new ToggleGroup();
            radioBtn1.setToggleGroup(radiobtnGroup);
            radioBtn2.setToggleGroup(radiobtnGroup);
            radioBtn1.setSelected(true);*/

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
